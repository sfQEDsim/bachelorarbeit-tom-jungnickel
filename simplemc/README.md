# simplemc

A simple and plain python implementation of a Monte-Carlo event generator for processes in
strong field QED. It is an experimental design to be included into the spectral cascade cycle.


## Virtual environments
It is stongly recommended to use virtual environments for development and productive runs. This encapsulates the package and all dependencies without messing up other python installations.  
#### Using [venv](https://docs.python.org/3/tutorial/venv.html) from the python standard library:  
1. Building the virtual environment:

    ```bash
    python -m venv <your_venv_name>
    ```

2. Activate the environment:

    ```bash
    source <your_venv_name>/bin/activate
    ```

3. To deactivate the virtual environment, just type  

    ```bash
    deactivate
    ```


#### Using [virtualenv](https://virtualenv.pypa.io/en/latest/):
1. Building the virtual environment:

    ```bash
    virtualenv <your_venv_name>
    ```

2. Activate the environment:

    ```bash
    source <your_venv_name>/bin/activate
    ```

3. To deactivate the virtual environment, just type  

    ```bash
    deactivate
    ```


#### Using [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv):
1. Building the virtual environment:

    ```bash
    pyenv virtualenv 3.8 <your_venv_name>
    ```

2. Activate the environment in the local directory:

    ```bash
    pyenv local <your_venv_name>
    ```

    This will create a hidden file `.python_version` containing a 'link' to the actual virtual environment managed by pyenv.


3. To 'deactivate' the virtual environment just remove this hidden file:

    ```bash
    rm .python_version
    ```

## Install the required `qftlib`
Since the essential package `qftlib` is not registered to PyPi yet, we need to grab it from source. Activate your virtual environment, and clone the `qftlib` repo somewhere in the environment source tree:
```bash
    git clone https://gitlab.hzdr.de/sfQEDsim/qftlib.git
     cd qftlib
```
Since `qftlib` is in experimental status as well, the master branch is nearly empty, so we need to checkout the dev branch of the `qftlib` repo:
```bash
git checkout dev
```
followed by the actual installation of `qftlib`:
```bash
pip install [-e] .[all,dev,docs]
```
where the flag `-e` means the package is directly linked into the python site-packages **without** a copy.
The options `[all,dev,docs]` refer to the requirements defined in the `options.extras_require` section in `setup.cfg`.
One shall **not** use `python setup.py install`, since the file `setup.py` will not be present for every build of the package.  
To test, if everything works properly, just run `pytest` in the root of the `qftlib` source:
```bash
pytest
```
Make sure, that you use the correct `pytest` (it needs to be in the same virtual environment as your `qftlib` installation, see [Troubleshooting: pytest with virtual environments](#pytest_venv)).


## Install `simplemc`
Installing `simplemc` it is recommended to use [pip](https://pip.pypa.io/en/stable/) (in the activated environment, see above). Since `simplemc` is also not registered to PyPi yet, we need to do the same procedure as for the `qftlib`.

#### Grab the repository
Go to the directory you want to have the source of `simplemc` and activate the virtual environment, where you have installed the `qftlib`.
In order to get the source of latest release, just clone the master branch from hzdr.gitlab:
```bash
git clone https://gitlab.hzdr.de/sfQEDsim/simplemc.git  
 cd simplemc
```
To get the latest development version, just checkout to the dev branch:
```bash
git checkout dev
```
and it is recommended to always pull the latest commit:
```bash
git pull origin dev
```
#### Installation
The installation itself is as easy as for `qftlib`. Go to the root directory of the `simplemc` source and hit
```bash
pip install -e .
```
### Troubleshooting: pytest with virtual environments <span id="pytest_venv"><span>
After installation, the restart of your virtual environment might be necessary, since the `pytest` command uses the `PYTHONPATH` which is not automatically changed to your venv.  

```bash
deactivate && source <your_venv_name>/bin/activate
```

or just run `hash -r` instead.
