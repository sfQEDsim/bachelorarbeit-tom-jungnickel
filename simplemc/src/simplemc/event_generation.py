"""
This module contains the Monte-Carlo event generator.
"""

import attr
import numpy as np
import copy
from .defaults import DEFAULTS
from .utils import _get_seed_list, generate_rnd_momentum, seed_generator
from .event_records import EventRecord, EventRecordList, event_converter
from .particle_records import ParticleRecord,ParticleRecordList
from .field_records import FieldRecord

__all__ = ["EventGenerator"]

def single_identity(particle_record,field_record,seed):
    """Process where nothing happens.

    the particle is just transfered from incoming to outgoing. The returned en_frac is zero
    """
    out_particle = ParticleRecord(particle_record.mom,is_incoming=False,is_anti_particle=particle_record.is_anti_particle,charge = particle_record.charge,mass = particle_record.mass)
    en_frac = 0.0 # nothing is absorbed
    return out_particle, en_frac


def single_artificial_compton(particle_record,field_record,seed):
    """
    artificial compton-like process.

    Returns
    Notes
    -----
        - builds a photon if the input particle is charged.
        - randomly generated the momenta for electron and photon.
        - the new electron momentum is scaled in a way, that the decreasing of the electron energy is visible
        - the new photon energy is scaled in a way, that the resulting energy fractions parameter are around 1
    """
    if particle_record.charge==0:
        return single_identity(particle_record,field_record,seed)
    seed1, seed2 = _get_seed_list(2,seed)
    np.random.seed(seed1)
    mom1 = generate_rnd_momentum(seed = seed1,mass=1,scale=particle_record.mom.x)
    OutElectron1 = ParticleRecord(mom1,is_incoming= False,charge=1)
    mom2 = generate_rnd_momentum(seed = seed2,mass = 0,scale=field_record.mom.E*2)
    OutPhoton2 = ParticleRecord(mom2,is_incoming= False,charge=0)

    tot_out_mom = mom1 + mom2
    en_frac = (tot_out_mom@tot_out_mom - particle_record.mass**2)/(2*particle_record.mom@field_record.mom)

    return [OutElectron1,OutPhoton2],en_frac



AVAIL_PROCESSES = [single_identity,single_artificial_compton]

def choose_rnd_process(seed,compton_weight = None):
    """Helper function.

    randomly choose a process from AVAIL_PROCESSES, where the second process is weighted with `compton_weight`.

    .. todo::
        - needs to be extended to arbitrary number of processes
        - needs to be extended to total cross sections as weight mechanism.
    """
    np.random.seed(seed)
    if compton_weight is None:
        compton_weight = 0.1
    rnd_proc_id = int(np.random.uniform()<=compton_weight)
    return AVAIL_PROCESSES[rnd_proc_id]

def build_new_particle_record(particle_record,field_record,seed):
    """Helper function.

    build a new particle record with a randomly choosen process and randomly choosen out_momenta.
    """
    proc = choose_rnd_process(seed = seed)
    out_momenta, en_frac = proc(particle_record,field_record,seed)
    return out_momenta, en_frac


def particle_update(event_record,seed):
    """Particle Updater.

    Build a new EventRecord from the input EventRecord, where for each incoming particle, a random process is choosen and for each of these processes, the out momenta are randomly determined.
    All outgoing particles are collected and summarized to a new EventRecord.
    For this new EventRecord, the energy fractions transferred from the field (for each process that happened in this event) are calculated and stored in event_record.en_fracs.
    """
    np.random.seed(seed)
    for particle_record in event_record.incoming_particles.records:
        local_seed = np.random.randint(10000000,99999999)
        out_particles,en_frac = build_new_particle_record(particle_record,event_record.in_field,local_seed)
        event_record.add_outgoing_particle(out_particles)
        event_record.add_en_frac(en_frac)
    return event_record

def field_update(event_record):
    """Field Updater

    Creates a new field out_field as a copy of in_field and reduces it according to the chosen method.
    Then adds the new out_field to the event_record.
    """
    in_field_spec = event_record.in_field.spectrum
    out_field_spec = copy.deepcopy(in_field_spec)
    out_field_spec.update(event_record)
    event_record.out_field = FieldRecord(event_record.in_field.mom, out_field_spec, event_record.in_field.spectrum_bounds)
    return event_record

def transform_outgoing_to_incoming(particle_record_list):
    """Helper function.

    Builds a new ParticleRecordList, where the outgoing particles of the input are the incoming particles of the output.
    """
    out = []
    for particle_record in particle_record_list.records:
        out.append(ParticleRecord(particle_record.mom,is_incoming=True,is_anti_particle=particle_record.is_anti_particle,charge = particle_record.charge,mass = particle_record.mass))
    return ParticleRecordList(out)


@attr.s(slots=True,repr=False)
class EventGenerator:
    init_event = attr.ib(converter = event_converter)
    current_event = attr.ib(init=False)
    no_produced_events = attr.ib(init=False,default = 0)
    max_no_events = attr.ib(default=DEFAULTS.max_no_events)
    global_seed = attr.ib(default = DEFAULTS.seed)
    __seed_generator = attr.ib(init=False)

    @current_event.default
    def current_event_default(self):
        return self.init_event

    @__seed_generator.default
    def __seed_generator_default(self):
        return seed_generator(seed = self.global_seed)

    def __build_new_event(self):
        if self.current_event.init_event:
            new_event = EventRecord(self.current_event.incoming_particles.records,self.current_event.in_field)
        else:
            new_event = EventRecord(transform_outgoing_to_incoming(self.current_event.outgoing_particles).records,self.current_event.out_field)
        current_seed = next(self.__seed_generator)
        particle_update(new_event,seed = current_seed)
        field_update(new_event)
        self.current_event = new_event
        return self.current_event

    def __iter__(self):
        return self

    def __next__(self):
        if self.no_produced_events<self.max_no_events and (self.current_event.stop_flag==False):
            # Will stop after field got below 0, so last the record is 'invalid'
            self.no_produced_events+=1
            return self.__build_new_event()
        raise StopIteration()
