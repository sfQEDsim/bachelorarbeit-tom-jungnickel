"""
This module contains the class describing the event records.
"""

import attr
import numpy as np
from .particle_records import ParticleRecordList

__all__ = ["EventRecord","EventRecordList"]

def _event_array_converter(event_list):
    """Helper functiion.

    Converts an event record to an array of records if necessary.
    """
    return np.atleast_1d(event_list)

def _get_event_record_string(event_record):
    """Helper function.

    Build an info string of an EventRecord.
    """

    header = f"EVENT RECORD (id: {event_record.id})\n"
    header+= "#"*(len(header)-2)
    header+="\n"
    header+= f"Number incoming particles: {len(event_record.incoming_particles)}\n"
    header+= f"Number outgoing particles: {len(event_record.outgoing_particles)}\n"
    incoming = "INCOMING PARTICLES\n"
    incoming+= "==================\n"
    incoming+= str(event_record.incoming_particles)
    outgoing = "OUTGOING PARTICLES\n"
    outgoing+= "==================\n"
    outgoing+= str(event_record.outgoing_particles)

    in_field = "INCIDENT FIELD\n"
    in_field+= "==============\n"
    in_field+= str(event_record.in_field)

    out_field = "UPDATED FIELD\n"
    out_field+= "==============\n"
    out_field+= str(event_record.out_field)

    en_fracs = "ENERGY FRACTIONS\n"
    en_fracs+= "================\n"
    en_fracs+= str(event_record.en_fracs)
    return "\n".join([header,incoming,outgoing,in_field,out_field,en_fracs]) + "\n"

def event_converter(event_like):
    """Helper function.

    Convert the event-like input to an EventRecord if necessary.
    """
    if isinstance(event_like,EventRecord):
        return event_like
    else:
        return EventRecord(event_like)


@attr.s
class EventRecord:
    incoming_particles : ParticleRecordList = attr.ib(converter = ParticleRecordList)
    in_field : "FieldRecord" = attr.ib()
    outgoing_particles : ParticleRecordList = attr.ib(init=False)
    out_field : "FieldRecord" = attr.ib(init=False)
    en_fracs = attr.ib(init=False)
    stop_flag = attr.ib(default=False)

    id = attr.ib(init=False)

    @outgoing_particles.default
    def __outgoing_particles_default(self):
        return ParticleRecordList()

    @en_fracs.default
    def __en_fracs_default(self):
        return np.array([],dtype=float)

    @id.default
    def __id_default(self):
        return id(self)

    @property
    def init_event(self):
        return not bool(len(self.outgoing_particles))

    def add_outgoing_particle(self,particle_list):
        self.outgoing_particles.add_records(particle_list)

    def add_en_frac(self,en_frac):
        self.en_fracs = np.append(self.en_fracs,en_frac)

    def __str__(self):
        return _get_event_record_string(self)

@attr.s
class EventRecordList:
    records : np.ndarray = attr.ib(converter = _event_array_converter)
    id = attr.ib(init=False,repr=False)

    @records.default
    def __records_defaults(self):
        return np.array([],dtype=object)

    def add_records(self,records):
        record_list = _event_array_converter(records)
        self.records = np.append(self.records, record_list)

    def __len__(self):
        return len(self.records)

    @id.default
    def id_default(self):
        """The id of this instance is used from the python object"""
        return id(self)

    def __str__(self):
        header = f"""Event Records (id:{self.id},no. of events: {len(self)})"""
        header_lines = "#"*len(header)
        header = "\n".join([header_lines,header,header_lines])
        header+= "\n\n"
        event_rec_string = "\n".join([f"Event {i}:\n{rec.__str__()}" for i,rec in enumerate(self.records)])
        return "".join([header,event_rec_string])
