"""
This module provides classes for Particle records and lists of these.
"""
from typing import Any, Optional
import itertools

import attr
import numpy as np
from qftlib.Momentum import _FourMomentumType


__all__ = ["ParticleRecord","ParticleRecordList"]

def _particle_array_converter(particle_list):
    """Helper functiion.

    Converts a particle record to an array of records if necessary.

    """
    return np.atleast_1d(particle_list)

@attr.s(frozen=True)
class ParticleRecord:
    mom : _FourMomentumType = attr.ib()
    mass : float = attr.ib()
    charge : Optional[float] = attr.ib(default = None)
    is_incoming: bool = attr.ib(default=True, kw_only=True)
    is_outgoing: bool = attr.ib(init=False)
    is_anti_particle: bool = attr.ib(default=False, kw_only=True)
    id : int = attr.ib(init=False,repr=False)

    @mom.validator
    def __mom_onshell_validator(self, attribute: Any, value: Any) -> None:
        assert (
            self.mom.isonshell
        ), "Particle momentum needs to be on-shell. {self.mom*self.mom} != {self.mass**2}"

    @is_incoming.validator
    def __is_incoming_validator(self, attribute: Any, value: Any) -> None:
        assert isinstance(
            value, bool
        ), f"The keyword argument `is_incoming` needs to be a boolian. {type(value)} given."

    @is_anti_particle.validator
    def __is_anti_particle_validator(self, attribute: Any, value: Any) -> None:
        assert isinstance(
            value, bool
        ), f"The keyword argument `is_anti_particle` needs to be a boolian. {type(value)} given."

    @is_outgoing.default
    def __outgoing_default(self) -> bool:
        return not self.is_incoming

    @mass.default
    def __mass_default(self):
        return self.mom.mass

    @id.default
    def id_default(self):
        """The id of this instance is used from the python object"""
        return id(self)

    def __str__(self):
        header = f"Particle (id:{self.id}) | mass: {self.mass}, charge: {self.charge}"
        direction = str("incoming" if self.is_incoming else "outgoing")
        mom = f"four momentum: ({direction})\n"
        mom+=f"\t E: {float(self.mom.E)}\n"
        mom+=f"\tpx: {float(self.mom.x)}\n"
        mom+=f"\tpy: {float(self.mom.y)}\n"
        mom+=f"\tpz: {float(self.mom.z)}\n"
        return "\n".join([header,mom])



@attr.s
class ParticleRecordList:
    records : np.ndarray = attr.ib(converter = _particle_array_converter)
    id = attr.ib(init=False,repr=False)

    @records.default
    def __records_defaults(self):
        return np.array([],dtype=object)

    def add_records(self,records):
        record_list = _particle_array_converter(records)
        self.records = np.append(self.records, record_list)

    def __len__(self):
        return len(self.records)

    @id.default
    def id_default(self):
        """The id of this instance is used from the python object"""
        return id(self)

    def __str__(self):
        header = f"""Particle Records (id:{self.id},no. of particles: {len(self)})\n"""
        header+= "-"*(len(header)-2)
        header+= "\n\n"
        particles = "\n".join([rec.__str__() for rec in self.records])
        return "".join([header,particles])
