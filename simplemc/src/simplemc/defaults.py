"""
Definition of global default values
"""
import attr
import numpy as np

def scaled_exp(x,l=0.01):
    """
    scaled exponential functions with `scaled_exp(0)=1` and `scaled_exp(1)=0`
    For scale parameter l << 1, the function has highly exponential behavior, for l -> 1, the function becomes linear.
    """
    return (1-l)*np.exp(np.log(l)*x) + l

def scaled_exp_generator(size):
    return scaled_exp(np.random.random(size),l=0.1)

@attr.s(frozen = True)
class DEFAULTS:
    seed = attr.ib(init = False, default = 12345678)
    max_no_events = attr.ib(init = False, default = 5)
    spectrum_bounds = attr.ib(init=False,default = (-1,1))
    mom_scale = attr.ib(init=False,default = 1.0)
    rnd_generator = attr.ib(init=False,default = scaled_exp_generator)

DEFAULTS = DEFAULTS() # overwrites the class so it cannot be initialised again
