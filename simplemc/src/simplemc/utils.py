"""
This module contains some utility functions for random generation of seeds, processes and momenta.
"""
import numpy as np
import qftlib as qft

from .defaults import DEFAULTS


__all__ = ["seed_generator","generate_rnd_momentum"]

def seed_generator(seed = None):
    """Generate an iterator of random seeds from an initial seed. Each returned seed is an eight digit integer"""
    if seed is None:
        seed = DEFAULTS.seed
    np.random.seed(seed)
    while True:
        yield np.random.randint(10000000,99999999)

def _get_seed_list(n,seed):
    np.random.seed(seed)
    return list(np.random.randint(10000000,99999999,n))

def generate_rnd_momentum(seed,mass=None,rnd_generator=None, scale = None):
    """Generates one random onshell four momentum.

    :param seed: Seed for the random generation. Needs to be passed, since the default seed would produce the same momentum over and over again.
    :type seed: int

    :param mass: Mass related to the generated four momentum. For every randomly generated momentum `mom`, the onshell condition `mass**2==mom@mom` is always fulfilled.
    :type mass: numbers.Real

    :param rnd_generator: The random number generator. Needs to have the signature `rnd_generator(size: Union[Tuple,int])->numbers.Real`, with the kwarg `size`.
    :type rnd_generator: callable

    :param scale: 
    """
    np.random.seed(seed)

    if scale is None:
        scale = DEFAULTS.mom_scale

    if rnd_generator is None:
        rnd_generator = DEFAULTS.rnd_generator

    if mass is None:
        mass = np.random.random()

    px = rnd_generator(size=(1,))*scale
    pz = rnd_generator(size=(1,))*scale
    py = rnd_generator(size=(1,))*scale
    E = np.sqrt(px**2 + py**2 + pz**2 + mass**2)
    return qft.FourMomentum(E,px,py,pz,mass=mass)
