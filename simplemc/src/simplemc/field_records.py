"""
This module contains the field record and lists of that.
"""
import attr
import numpy as np
from qftlib.Momentum import _FourMomentumType
from typing import Callable, Any
from .defaults import DEFAULTS

from scipy.integrate import quad

__all__ = ["FieldRecord"]

@attr.s(frozen=True)
class FieldRecord:
    mom : _FourMomentumType = attr.ib()
    spectrum: Callable = attr.ib() # = |F(l)|^2
    spectrum_bounds = attr.ib()
    total_energy = attr.ib(init=False)
    id : int = attr.ib(init=False,repr=False)

    @mom.validator
    def __mom_onshell_validator(self, attribute: Any, value: Any) -> None:
        assert (
            self.mom.mass == 0
        ), f"Field reference momentum needs to be massless. {self.mom.mass**2}"

        assert (
            self.mom.isonshell
        ), f"Particle momentum needs to be on-shell. {self.mom*self.mom} != {0}"

    @spectrum_bounds.default
    def __spectrum_bounds_default(self):
        return DEFAULTS.spectrum_bounds

    @total_energy.default
    def __total_energy_default(self):
        """
        U = omega * int dl |F(l)|^2
        """
        return self.spectrum.calculate_energy(self) ##(self.spectrum_bounds)

    @id.default
    def id_default(self):
        """The id of this instance is used from the python object"""
        return id(self)

    def __str__(self):
        header = f"Field Record (id:{self.id}) "
        mom = f"reference four momentum: "
        mom+=f"(E: {float(self.mom.E)}, "
        mom+=f"px: {float(self.mom.x)}, "
        mom+=f"py: {float(self.mom.y)}, "
        mom+=f"pz: {float(self.mom.z)})"
        tot_en = f"total energy: {self.total_energy}\n"
        return "\n".join([header,mom,tot_en])
