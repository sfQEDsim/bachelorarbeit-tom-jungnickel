#!/usr/bin/env python
# Copyright (c) 2021, Uwe Hernandez Acosta
#
# Distributed under the 3-clause BSD license, see accompanying file LICENSE
# or https://gitlab.hzdr.de/sfQEDsim/simplemc for details.

from setuptools import setup

setup()

# This file is optional, on recent versions of pip you can remove it and even
# still get editable installs.
