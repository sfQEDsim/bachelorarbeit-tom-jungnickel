.. currentmodule:: qftlib.Lorentz

#################
LorentzVectorType
#################
The type :class:`_LorentzVectorType` contains all informations of an element of a Minkowski space.
Since this is vectorized, each component of a :class:`_LorentzVectorType` can be an arbitrary array_like container,
e.g. a :class:`numpy.ndarray`, but also a :class:`qftlib.DiracMatrix` or a :class:`numbers.Number`.


.. autoclass:: _LorentzVectorType
    :members:
    :show-inheritance:
    :private-members:
    :inherited-members:
    :special-members:
