#########################
LorentzVector constructor
#########################
The python function ``LorentzVector`` tries to infer the input in order to construct a :class:`qftlib._LorentzVectorType`.


.. autofunction:: qftlib.Lorentz.LorentzVector
