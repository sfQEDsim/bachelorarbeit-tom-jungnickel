##################
qftlib Reference
##################

:Release: |version|
:Date: |today|

This is the API reference of qftlib. Here all exposed functions and classes will be documented.

.. toctree::
   :maxdepth: 3

   Dirac algebra <DiracAlgebra/index>
   Minkowski space <MinkowskiSpace/index>
   Four-momentum <FourMomentum/index>
   Polarisation <Polarisation/index>
