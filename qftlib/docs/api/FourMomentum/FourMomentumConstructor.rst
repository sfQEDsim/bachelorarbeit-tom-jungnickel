#########################
LorentzVector constructor
#########################
The python function ``FourMomentum`` tries to infer the input in order to construct a :class:`qftlib._FourMomentumType`, which calls different custom constructors.

.. autofunction:: qftlib.Momentum._from_LorentzVector
.. autofunction:: qftlib.Momentum._from_tuple
.. autofunction:: qftlib.Momentum._from_ndarray
.. autofunction:: qftlib.Momentum._from_hepVector
