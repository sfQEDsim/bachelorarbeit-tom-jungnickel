.. currentmodule:: qftlib.Momentum

################
FourMomentumType
################
Numeric type to describe the four-momentum of a particle.


.. autoclass:: _FourMomentumType
    :members:
    :show-inheritance:
    :private-members:
    :inherited-members:
    :special-members:
