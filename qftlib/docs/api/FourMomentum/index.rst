############
FourMomentum
############

Minkowski Space calculations.

.. toctree::


   FourMomentum type <FourMomentumType>
   FourMomentum constructor <FourMomentumConstructor>
