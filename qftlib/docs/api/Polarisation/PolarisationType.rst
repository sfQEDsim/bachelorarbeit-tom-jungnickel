.. currentmodule:: qftlib.Polarisation

#################
Polarisation type
#################



.. autoclass:: _PolarisationVectorType
    :members:
    :show-inheritance:
    :private-members:
    :inherited-members:
    :special-members:
