########################
Polarisation constructor
########################
The python function ``PolarisationVector`` tries to infer the input in order to construct a :class:`qftlib._PolarisationVectorType`.


.. autofunction:: qftlib.Polarisation.PolarisationVector
