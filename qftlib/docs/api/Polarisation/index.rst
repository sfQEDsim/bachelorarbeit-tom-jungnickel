############
Polarisation
############

Polarisations of fields

.. toctree::


   Polarisation type <PolarisationType>
   Polarisation constructor <PolarisationContructor>
