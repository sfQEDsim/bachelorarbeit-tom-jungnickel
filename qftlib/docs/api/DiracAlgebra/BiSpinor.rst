#############################
BiSpinors and AdjointBiSpinor
#############################

.. autoclass:: qftlib.BiSpinor
    :members:
    :show-inheritance:
    :private-members:
    :inherited-members:
    :special-members:

.. autoclass:: qftlib.AdjointBiSpinor
    :members:
    :show-inheritance:
    :private-members:
    :inherited-members:
    :special-members:
