############
DiracAlgebra
############

.. automodule:: qftlib.DiracAlgebra

.. toctree::
   :caption: Module content

   Abstract base class <DiracABC>
   Dirac matrices <DiracMatrix>
   Spinors <BiSpinor>
