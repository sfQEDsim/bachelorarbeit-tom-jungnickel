#########################################
Abstract base class for the Dirac algebra
#########################################

|:warning:| This class is only for subclassing. Do not initialize this class! |:warning:|

.. autoclass:: qftlib.DiracAlgebra._DiracABC
    :members:
    :show-inheritance:
    :private-members:
    :inherited-members:
    :special-members:
