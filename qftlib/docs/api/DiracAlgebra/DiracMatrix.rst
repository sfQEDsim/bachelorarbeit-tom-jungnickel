###########
DiracMatrix
###########
Dirac matrices are the matrix representations of linear mappings on spinor spaces.


.. autoclass:: qftlib.DiracMatrix
    :members:
    :show-inheritance:
    :private-members:
    :inherited-members:
    :special-members:
