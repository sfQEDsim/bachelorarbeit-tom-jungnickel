# Usage of qftlib
## Dirac's Algebra


```
import numpy as np
from qftlib import DiracMatrix, BiSpinor

d = np.random.random(4,4)
D = DiracMatrix(d)

s = np.random.random(4)
S = BiSpinor(s)

# product of a DiracMatrix and a BiSpinor
prod_DM_BS = D*S # = D@S
```
