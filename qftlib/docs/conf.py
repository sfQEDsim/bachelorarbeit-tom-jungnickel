# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# Warning: do not change the path here. To use autodoc, you need to install the
# package first.

from typing import List

from pkg_resources import get_distribution

# -- Project information -----------------------------------------------------

project = "qftlib"
copyright = "2021, Uwe Hernandez Acosta"
author = "Uwe Hernandez Acosta"

version = get_distribution(project).version
# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autosummary",
    "numpydoc",
    "myst_parser",
    "sphinx.ext.autodoc",
    "sphinx.ext.mathjax",
    "sphinx.ext.napoleon",
    "sphinx_copybutton",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinxemoji.sphinxemoji",
]

# Intersphinx configuration
intersphinx_mapping = {
    "neps": ("https://numpy.org/neps", None),
    "python": ("https://docs.python.org/dev", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "scipy": ("https://docs.scipy.org/doc/scipy/reference", None),
    "matplotlib": ("https://matplotlib.org/stable", None),
    "pytest": ("https://docs.pytest.org/en/stable", None),
}

numpydoc_show_class_members = False

# Display todos by setting to True
todo_include_todos = True

# use consistent emoji style
sphinxemoji_style = "twemoji"

# Add any paths that contain templates here, relative to this directory.
templates_path = []

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "**.ipynb_checkpoints", "Thumbs.db", ".DS_Store", ".env"]

pygments_style = "sphinx"
# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
# html_theme = "sphinx_book_theme"

# html_title = f"qftlib {version[0:3]}"

html_baseurl = "https://qftlib.readthedocs.io/en/latest/"

# for sphinx_book_theme
# html_theme_options = {
#    "home_page_in_toc": True,
#    "repository_url": "https://gitlab.hzdr.de/sfQEDsim/qftlib",
#    "use_repository_button": True,
#    "use_issues_button": True,
#    "use_edit_page_button": True,
# }

# for RTD theme
html_theme_options = {
    "logo_only": False,
    "display_version": True,
    "prev_next_buttons_location": "bottom",
    "style_external_links": False,
    "vcs_pageview_mode": "",
    #'style_nav_header_background': 'white',
    # Toc options
    "collapse_navigation": True,
    "sticky_navigation": True,
    "navigation_depth": 4,
    "includehidden": True,
    "titles_only": False,
}
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path: List[str] = []
