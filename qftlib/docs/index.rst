
####################
qftlib documentation
####################


Introduction
------------
qftlib is a library for numerical computations of scattering matrix elements found in quantum field theory.
It uses numpy for vectorized calculations and has build-in types Lorentz four vectors and Dirac Spinors and Matrices.

Furthermore, qftlib contains the algebraic expressions used in the Feynman diagram technique, i.e. Feynman rules for external legs, vertices and propagates of fermions and bosons respectively. For now, only the QED Feynman rules are present, but the underlying system is capable to be applied to other quantum field theories as well.

.. toctree::
   :maxdepth: 4
   :hidden:

   User Guide <usage/intro>
   API reference <api/index>
   TODO list <TODO>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
