[metadata]
name = qftlib
description = Python library for manipulation of Minkowski vectors, Dirac matrices and spinors.
long_description = file: README.md
long_description_content_type = text/markdown
url = https://gitlab.hzdr.de/hernan68/qftlib
author = Uwe Hernandez Acosta
author_email = u.hernandez@hzdr.de
maintainer = Uwe Hernandez Acosta
maintainer_email = u.hernandez@hzdr.de
license = BSD-3-Clause
license_file = LICENSE
platforms =
    Any
classifiers =
    Development Status :: 1 - Planning
    Intended Audience :: Developers
    Intended Audience :: Science/Research
    License :: OSI Approved :: BSD License
    Operating System :: OS Independent
    Programming Language :: Python
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3 :: Only
    Programming Language :: Python :: 3.8
    Programming Language :: Python :: 3.9
    Topic :: Scientific/Engineering
project_urls =
    Documentation = https://qftlib.readthedocs.io/
    Bug Tracker = https://gitlab.hzdr.de/sfQEDsim/qftlib/issues
    Discussions = https://gitlab.hzdr.de/sfQEDsim/qftlib/discussions
    Changelog = https://gitlab.hzdr.de/sfQEDsim/qftlibreleases

[options]
packages = find:
install_requires =
    numpy>=1.20.3
    vector
python_requires = >=3.8
include_package_data = True
package_dir =
    =src

[options.packages.find]
where = src

[options.extras_require]
dev =
    pytest>=4.6
    pytest-cov>=2.12.0
docs =
    Sphinx
    matplotlib>=3.4.2
    myst-parser
    numpy>=1.13.3
    numpydoc
    pytest>=4.6
    pytest-cov>=2.12.0
    sphinx-book-theme
    sphinx-rtd-theme
    sphinx_copybutton
    sphinxemoji
test =
    pytest>=4.6
    pytest-cov>=2.12.0

[tool:pytest]
addopts = -ra -Wd
    --cov-config=.coveragerc
    --cov qftlib
    --cov-report term-missing
    --cov-report html
testpaths =
    tests

[tool:isort]
profile = black
multi_line_output = 3

[check-manifest]
ignore =
    .github/**
    docs/**
    .pre-commit-config.yaml
    .readthedocs.yml
    src/*/version.py

[flake8]
ignore = E128, E203, E231, E501, E722, W503, B950
select = C,E,F,W,T,B,B9,I
per-file-ignores =
    tests/*: T
docstring-convention = numpy

[mypy]
files = src
python_version = 3.8
warn_unused_configs = True
disallow_any_generics = False
disallow_subclassing_any = False
disallow_untyped_calls = True
disallow_untyped_defs = True
disallow_incomplete_defs = True
check_untyped_defs = True
disallow_untyped_decorators = True
no_implicit_optional = True
warn_redundant_casts = True
warn_unused_ignores = True
warn_return_any = True
no_implicit_reexport = False
strict_equality = True

[mypy-numpy]
ignore_missing_imports = True

[mypy-scipy]
ignore_missing_imports = True
