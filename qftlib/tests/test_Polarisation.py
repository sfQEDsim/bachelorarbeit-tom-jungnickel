"""
Test suites for Four Polarisation.
"""
import numpy as np
from conftest import assert_call, build_input_array
from numpy.testing import assert_, assert_equal, assert_raises

import qftlib as qft
from qftlib.Lorentz import _LorentzVectorType
from qftlib.Polarisation import PolarisationVector, _PolarisationVectorType


# helper function -> maybe ship to conftest.py
def build_input(shape):
    return build_input_array((4,), shape)


# test subclassing
def test_subclass():
    assert_(issubclass(_PolarisationVectorType, _LorentzVectorType))


#######
# init tests
#######
# # does not test numerical correctness (which is tested in test_Lorentz.py)
def test_init_from_components(Shape):
    temp_arr = build_input(Shape)
    assert_call(PolarisationVector, temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3])
    temp = PolarisationVector(temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3])
    assert_(isinstance(temp, _PolarisationVectorType))

    groundtruth = {
        "x0": temp_arr[0],
        "x1": temp_arr[1],
        "x2": temp_arr[2],
        "x3": temp_arr[3],
    }
    for comp_name, value in groundtruth.items():
        assert_equal(getattr(temp, comp_name), value)


def test_init_from_array(Shape):
    temp_arr = build_input(Shape)
    assert_call(PolarisationVector, temp_arr)
    temp = PolarisationVector(temp_arr)
    assert_(isinstance(temp, _PolarisationVectorType))
    assert_equal(np.asarray(temp), temp_arr)


def test_init_from_Polarisation(Shape):
    temp_arr = build_input(Shape)
    assert_call(PolarisationVector, PolarisationVector(temp_arr))


def test_init_fail_array(Shape):
    temp_arr = build_input_array(
        (
            1,
            1,
        ),
        Shape,
    )
    assert_raises(ValueError, PolarisationVector, temp_arr)
    assert_raises(ValueError, PolarisationVector, None)


####
# test normalisation
####


def test_normalisation(Shape):
    one = np.ones(Shape)
    zero = np.zeros(Shape)
    eps = PolarisationVector(zero, zero, one, zero)
    eps_not_normed = PolarisationVector(zero, one, one, zero)
    assert_(eps.isnormed)
    assert_(not eps_not_normed.isnormed)


####
# test conjugation
####


def test_conjugation(Shape):
    one = np.ones(Shape, dtype=np.complex)
    zero = np.zeros(Shape, dtype=np.complex)
    eps = PolarisationVector(zero, zero, one + 1j * one, zero)
    groundtruth = PolarisationVector(zero, zero, one - 1j * one, zero)

    # with copy
    assert_equal(np.asarray(eps.conjugate()), np.asarray(groundtruth))

    # inplace
    eps.conj()
    assert_equal(np.asarray(eps), np.asarray(groundtruth))


def test_interface_to_fourmomenta(Shape):
    one = np.ones(Shape)
    zero = np.zeros(Shape)
    eps = PolarisationVector(zero, one, zero, zero)
    mom = qft.FourMomentum(build_input(Shape))

    assert_equal(mom @ eps, -mom.x)
