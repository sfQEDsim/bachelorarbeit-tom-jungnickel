"""
Test suites for Four Momenta.
"""
import numpy as np
import pytest
import vector
from conftest import assert_call, build_input_array
from numpy.testing import assert_, assert_equal, assert_raises

from qftlib.Lorentz import LorentzVector, _LorentzVectorType
from qftlib.Momentum import FourMomentum, _FourMomentumType


# helper function -> maybe ship to conftest.py
def build_input(shape):
    return build_input_array((4,), shape)


# test subclassing
def test_subclass():
    assert_(issubclass(_FourMomentumType, _LorentzVectorType))


#######
# init tests
#######
# # does not test numerical correctness (which is tested in test_Lorentz.py)
def test_init_from_components(Shape):
    temp_arr = build_input(Shape)
    assert_call(FourMomentum, temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3])
    assert_call(
        FourMomentum, temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3], mass=1.0
    )
    temp = FourMomentum(temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3])
    assert_(isinstance(temp, _FourMomentumType))

    component_names = {"x0": "E", "x1": "x", "x2": "y", "x3": "z"}
    groundtruth = {
        "x0": temp_arr[0],
        "x1": temp_arr[1],
        "x2": temp_arr[2],
        "x3": temp_arr[3],
    }
    for comp, alias in component_names.items():
        assert_equal(getattr(temp, comp), getattr(temp, alias))
        assert_equal(getattr(temp, comp), groundtruth[comp])


def test_init_from_array(Shape):
    temp_arr = build_input(Shape)
    assert_call(FourMomentum, temp_arr)
    temp = FourMomentum(temp_arr)
    assert_(isinstance(temp, _FourMomentumType))
    assert_equal(np.asarray(temp), temp_arr)


def test_init_fail_array(Shape):
    temp_arr = build_input_array(
        (
            1,
            1,
        ),
        Shape,
    )
    assert_raises(ValueError, FourMomentum, temp_arr)
    assert_raises(ValueError, FourMomentum, None)


def test_init_from_LV(Shape):
    temp_arr = build_input(Shape)
    temp_LV = LorentzVector(temp_arr)
    assert_call(FourMomentum, temp_LV)
    assert_call(FourMomentum, temp_LV, mass=1.0)
    temp = FourMomentum(temp_LV)
    assert_(isinstance(temp, _FourMomentumType))


def test_init_from_FourMomentum(Shape):
    temp_arr = build_input(Shape)
    assert_call(FourMomentum, FourMomentum(temp_arr))
    assert_call(FourMomentum, FourMomentum(temp_arr), mass=1.0)


####
# init from hep-vectors, see https://github.com/scikit-hep/vector
####
def _build_VectorNumpy4D(*args):
    return vector.array(
        {
            "x": args[0],
            "y": args[1],
            "z": args[2],
            "t": args[3],
        }
    )


def _build_MomentumNumpy4D(*args):
    return vector.array(
        {
            "x": args[0],
            "y": args[1],
            "z": args[2],
            "E": args[3],
        }
    )


def _build_VectorArray4D(*args):
    return vector.arr(
        {
            "x": args[0],
            "y": args[1],
            "z": args[2],
            "t": args[3],
        }
    )


def _build_MomentumArray4D(*args):
    return vector.arr(
        {
            "x": args[0],
            "y": args[1],
            "z": args[2],
            "E": args[3],
        }
    )


classes = [
    _build_VectorNumpy4D,
    _build_MomentumNumpy4D,
    _build_VectorArray4D,
    _build_MomentumArray4D,
]
classids = tuple(fct.__name__ for fct in classes)


@pytest.fixture(params=classes, ids=classids)
def VectorArrayType(request):
    return request.param


def test_init_from_VectorArray(Shape, VectorArrayType):
    if len(Shape) != 0:
        temp_arr = build_input(Shape)
        V = VectorArrayType(temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3])
        assert_call(FourMomentum, V)
        assert_call(FourMomentum, V, mass=1.0)
        temp = FourMomentum(V)
        assert_(isinstance(temp, _FourMomentumType))


def _build_VectorObject4D(*args):
    return vector.obj(
        **{
            "x": args[0],
            "y": args[1],
            "z": args[2],
            "t": args[3],
        }
    )


def _build_MomentumObject4D(*args):
    return vector.obj(
        **{
            "x": args[0],
            "y": args[1],
            "z": args[2],
            "E": args[3],
        }
    )


classes = [_build_VectorObject4D, _build_MomentumObject4D]
classids = tuple(fct.__name__ for fct in classes)


@pytest.fixture(params=classes, ids=classids)
def VectorObjType(request):
    return request.param


def test_init_from_VectorObj(VectorObjType):
    temp_arr = build_input(tuple())
    V = VectorObjType(temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3])
    assert_call(FourMomentum, V)
    assert_call(FourMomentum, V, mass=1.0)
    temp = FourMomentum(V)
    assert_(isinstance(temp, _FourMomentumType))


####
# test mass
####
def test_init_mass(Shape):
    temp_mass = 1.2
    xyz = build_input_array((3,), Shape)
    x, y, z = xyz[0], xyz[1], xyz[2]
    E = np.sqrt(temp_mass ** 2 + x ** 2 + y ** 2 + z ** 2)

    temp = FourMomentum(E, x, y, z, mass=temp_mass)
    assert_(temp.isonshell)

    E2 = np.sqrt(temp_mass ** 2 + x ** 2 + y ** 2 + z ** 2 + 1)
    temp2 = FourMomentum(E2, x, y, z, mass=temp_mass)
    assert_(not temp2.isonshell)


def test_fail_mass():
    temp_arr = build_input(tuple())
    assert_raises(ValueError, FourMomentum, temp_arr, mass=-1)

    temp = FourMomentum(temp_arr)
    assert_raises(ValueError, lambda x: getattr(temp, x), "isonshell")
