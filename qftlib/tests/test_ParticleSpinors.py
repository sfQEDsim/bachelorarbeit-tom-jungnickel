"""
test suite for ParticleSpinor.py
"""
import itertools

import numpy as np
import pytest
from conftest import assert_call, build_rnd_momentum_components
from numpy.testing import assert_, assert_almost_equal, assert_equal, assert_raises

import qftlib as qft
from qftlib.ParticleSpinor import (
    FermionBaseSpinorList,
    FermionSpinor,
    IncomingAntiFermion,
    IncomingFermion,
    OutgoingAntiFermion,
    OutgoingFermion,
)


@pytest.fixture
def Mom():
    comps, m = build_rnd_momentum_components()
    return qft.FourMomentum(comps, mass=m)


# test init


def test_init_BaseSpinor():
    assert_call(FermionBaseSpinorList)
    assert_call(FermionBaseSpinorList, anti_particle=True)


def test_init_FermionSpinor(Mom):
    assert_call(FermionSpinor, Mom)
    assert_call(FermionSpinor, Mom, is_incoming=True)
    assert_call(FermionSpinor, Mom, is_anti_particle=True)
    assert_call(FermionSpinor, Mom, is_incoming=True, is_anti_particle=True)


# test constructors
def test_init_FermionConstructors(Mom):
    assert_call(IncomingFermion, Mom)
    assert_call(OutgoingFermion, Mom)
    assert_call(IncomingAntiFermion, Mom)
    assert_call(OutgoingAntiFermion, Mom)


def test_IncomingFermion_properties(Mom):
    IncomingFermion_obj = IncomingFermion(Mom)
    assert_(isinstance(IncomingFermion_obj, FermionSpinor))
    assert_(IncomingFermion_obj.is_incoming)
    assert_(not IncomingFermion_obj.is_anti_particle)


def test_OutgoingFermion_properties(Mom):
    OutgoingFermion_obj = OutgoingFermion(Mom)
    assert_(isinstance(OutgoingFermion_obj, FermionSpinor))
    assert_(not OutgoingFermion_obj.is_incoming)
    assert_(not OutgoingFermion_obj.is_anti_particle)


def test_IncomingAntiFermion_properties(Mom):
    IncomingAntiFermion_obj = IncomingAntiFermion(Mom)
    assert_(isinstance(IncomingAntiFermion_obj, FermionSpinor))
    assert_(IncomingAntiFermion_obj.is_incoming)
    assert_(IncomingAntiFermion_obj.is_anti_particle)


def test_OutgoingAntiFermion_properties(Mom):
    OutgoingAntiFermion_obj = OutgoingAntiFermion(Mom)
    assert_(isinstance(OutgoingAntiFermion_obj, FermionSpinor))
    assert_(not OutgoingAntiFermion_obj.is_incoming)
    assert_(OutgoingAntiFermion_obj.is_anti_particle)


# test behaviour base spinor
def test_base_orthonomality():
    B1 = FermionBaseSpinorList()
    B2 = FermionBaseSpinorList(anti_particle=True)
    for i, j in itertools.product([0, 1], [0, 1]):
        assert_equal(B1[i].vdot(B1[j]), 1.0 * (i == j))
        assert_equal(B2[i].vdot(B2[j]), 1.0 * (i == j))
        assert_equal(B1[i].vdot(B2[j]), 0.0)


def test_base_index_fail():
    B1 = FermionBaseSpinorList()
    assert_raises(IndexError, B1.__getitem__, 2)


# test othrogonality and norm of fermion spinors
def test_fermion_spinor_orthonomality(Mom):
    U = qft.IncomingFermion(Mom)
    Ub = qft.OutgoingFermion(Mom)
    for i, j in itertools.product([0, 1], [0, 1]):
        assert_almost_equal(Ub(i) * U(j), 2 * Mom.mass * (i == j))


# test othrogonality and norm of fermion spinors
def test_anti_fermion_spinor_orthonomality(Mom):
    Vb = qft.IncomingAntiFermion(Mom)
    V = qft.OutgoingAntiFermion(Mom)
    for i, j in itertools.product([0, 1], [0, 1]):
        assert_almost_equal(Vb(i) * V(j), -2 * Mom.mass * (i == j))


def check_diracs_equation_incoming_fermion(u, mom):
    return np.asarray(
        (qft.feynman_slash(mom) - mom.mass * qft.UnitDiracMatrix(len(mom.shape))) * u
    )


def check_diracs_equation_outgoing_fermion(u, mom):
    return np.asarray(
        u * (qft.feynman_slash(mom) - mom.mass * qft.UnitDiracMatrix(len(mom.shape)))
    )


def check_diracs_equation_outgoing_anti_fermion(u, mom):
    return np.asarray(
        (qft.feynman_slash(mom) + mom.mass * qft.UnitDiracMatrix(len(mom.shape))) * u
    )


def check_diracs_equation_incoming_anti_fermion(u, mom):
    return np.asarray(
        u * (qft.feynman_slash(mom) + mom.mass * qft.UnitDiracMatrix(len(mom.shape)))
    )


def test_DiracEquation_fermion(Mom):
    U = qft.IncomingFermion(Mom)
    Ub = qft.OutgoingFermion(Mom)
    for i in [0, 1]:
        assert_almost_equal(check_diracs_equation_incoming_fermion(U(i), Mom), 0.0)
        assert_almost_equal(check_diracs_equation_outgoing_fermion(Ub(i), Mom), 0.0)


def test_DiracEquation_anti_fermion(Mom):
    Vb = qft.IncomingAntiFermion(Mom)
    V = qft.OutgoingAntiFermion(Mom)
    for i in [0, 1]:
        assert_almost_equal(
            check_diracs_equation_incoming_anti_fermion(Vb(i), Mom), 0.0
        )
        assert_almost_equal(check_diracs_equation_outgoing_anti_fermion(V(i), Mom), 0.0)
