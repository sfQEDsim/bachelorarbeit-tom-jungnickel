"""
test suite for DiracAlgebra.py
"""
import operator as op

import numpy as np
import pytest
from conftest import Shape, build_broadcastable_arrays, build_input_array
from numpy.testing import assert_, assert_almost_equal, assert_equal, assert_raises

from qftlib.DiracAlgebra import AdjointBiSpinor, BiSpinor, DiracMatrix

# class fixture
classes = [DiracMatrix, BiSpinor, AdjointBiSpinor]

classids = tuple(cls.__name__ for cls in classes)


@pytest.fixture(params=classes, ids=classids)
def AlgebraClass(request):
    return request.param


# additional asserts
def assert_AlgebraObj_equal(DO1, DO2, msg=""):
    try:
        assert_(DO1.__class__ == DO2.__class__)
        assert_equal(DO1.data, DO2.data)
    except AssertionError:
        msg = f"Result {DO1}\nTarget {DO2}"
        raise AssertionError(msg)


def assert_AlgebraObj_almost_equal(DO1, DO2, msg=""):
    try:
        assert_(DO1.__class__ == DO2.__class__)
        assert_almost_equal(DO1.data, DO2.data)
    except AssertionError:
        msg = f"Result {DO1}\nTarget {DO2}"
        raise AssertionError(msg)


###########################
#
# Tests involving one class
#
###########################


def test_init(AlgebraClass, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    assert_(AlgebraClass, temp_arr)
    tempDO = AlgebraClass(temp_arr)
    assert_(type(tempDO.__repr__()) == str)
    assert_(type(tempDO.__str__()) == str)


def test_fail_init(AlgebraClass, Shape):
    temp_arr = build_input_array(
        (
            1,
            1,
        ),
        Shape,
    )
    assert_raises(ValueError, AlgebraClass, temp_arr)


def test_shapes(AlgebraClass, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    tempDO = AlgebraClass(temp_arr)
    assert_equal(tempDO.tshape, AlgebraClass.TSHAPE)
    assert_equal(tempDO.shape, Shape)


def test_asarray(AlgebraClass, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    tempDO = AlgebraClass(temp_arr)
    assert_equal(tempDO.data, temp_arr)
    assert_equal(tempDO.data, np.asarray(tempDO))


def test_reshape(AlgebraClass, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    tempDO = AlgebraClass(temp_arr)
    new_shape = tempDO.shape + (1,)
    res = tempDO.reshape(*new_shape)
    assert_equal(res.shape, new_shape)


unary_methods = [
    op.neg,
    op.pos,
]
unary_method_ids = tuple(mth.__name__ for mth in unary_methods)


@pytest.fixture(params=unary_methods, ids=unary_method_ids)
def UnaryMethod(request):
    return request.param


def test_unary_methods(AlgebraClass, UnaryMethod, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    tempDO = AlgebraClass(temp_arr)
    groundtruth = UnaryMethod(temp_arr)
    res = UnaryMethod(tempDO)
    assert_(isinstance(res, AlgebraClass))
    assert_equal(res.data, groundtruth)


# test arithmetics with numbers and ndarrays
def radd(x, y):
    return op.add(y, x)


def rsub(x, y):
    return op.sub(y, x)


def rmul(x, y):
    return op.mul(y, x)


binary_array_methods = [op.add, op.sub, op.mul, radd, rsub, rmul]
binary_array_method_ids = tuple(mth.__name__ for mth in binary_array_methods)


@pytest.fixture(params=binary_array_methods, ids=binary_array_method_ids)
def BinaryArrayMethod(request):
    return request.param


def test_binary_array_methods(AlgebraClass, BinaryArrayMethod, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    tempDO = AlgebraClass(temp_arr)
    other = np.random.random(Shape)
    res = BinaryArrayMethod(tempDO, other)
    groundtruth = BinaryArrayMethod(
        tempDO.data, other.reshape((1,) * len(AlgebraClass.TSHAPE) + Shape)
    )
    assert_(isinstance(res, AlgebraClass))
    assert_almost_equal(res.data, groundtruth)


def test_binary_number_methods(AlgebraClass, BinaryArrayMethod, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    tempDO = AlgebraClass(temp_arr)
    other = np.random.random()
    res = BinaryArrayMethod(tempDO, other)
    groundtruth = BinaryArrayMethod(tempDO.data, other)
    assert_(isinstance(res, AlgebraClass))
    assert_almost_equal(res.data, groundtruth)


# test arithmetics same class
binary_same_class_methods = [op.add, op.sub]

binary_same_class_method_ids = tuple(mth.__name__ for mth in binary_same_class_methods)


@pytest.fixture(params=binary_same_class_methods, ids=binary_same_class_method_ids)
def BinarySameClassMethod(request):
    return request.param


S1 = Shape
S2 = Shape


def test_binary_same_class_methods(AlgebraClass, BinarySameClassMethod, S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_arrays(AlgebraClass.TSHAPE, S1, S2)
    tempDO1 = AlgebraClass(temp_arr1)
    tempDO2 = AlgebraClass(temp_arr2)
    res = BinarySameClassMethod(tempDO1, tempDO2)
    groundtruth = BinarySameClassMethod(tempDO1.data, tempDO2.data)
    assert_(isinstance(res, AlgebraClass))
    assert_almost_equal(res.data, groundtruth)


# test arithmetics raise for different class
binary_class_fail_methods = [op.add, op.sub, radd, rsub]

binary_class_fail_method_ids = tuple(mth.__name__ for mth in binary_class_fail_methods)


@pytest.fixture(params=binary_class_fail_methods, ids=binary_class_fail_method_ids)
def BinaryClassFailMethod(request):
    return request.param


AC1 = AlgebraClass
AC2 = AlgebraClass


def test_binary_class_fail_methods(AC1, AC2, BinaryClassFailMethod):
    if not (AC1 is AC2):
        temp_arr1 = build_input_array(AC1.TSHAPE, tuple())
        tempDO1 = AC1(temp_arr1)
        temp_arr2 = build_input_array(AC2.TSHAPE, tuple())
        tempDO2 = AC2(temp_arr2)
        assert_raises(TypeError, BinaryClassFailMethod, tempDO1, tempDO2)


def test_mul_fail(AlgebraClass):
    temp_arr1 = build_input_array(AlgebraClass.TSHAPE, tuple())
    tempDO1 = AlgebraClass(temp_arr1)
    assert_raises(TypeError, op.mul, tempDO1, None)


# test equality


def test_equal_diff_class(AC1, AC2):
    temp_arr1 = np.ones(AC1.TSHAPE + tuple())
    tempDO1 = AC1(temp_arr1)
    temp_arr2 = np.ones(AC2.TSHAPE + tuple())
    tempDO2 = AC2(temp_arr2)
    if AC1 is AC2:
        assert_(tempDO1 == tempDO2)
    else:
        assert_(not (tempDO1 == tempDO2))


def test_equal_same_class(AC1, S1, S2):
    temp_arr1 = np.ones(AC1.TSHAPE + S1)
    tempDO1 = AC1(temp_arr1)
    temp_arr2 = np.ones(AC1.TSHAPE + S2)
    tempDO2 = AC1(temp_arr2)
    if np.array_equal(S1, S2):
        assert_(tempDO1 == tempDO2)
        assert_(not (tempDO1 != tempDO2))
    else:
        assert_(tempDO1 != tempDO2)
        assert_(not (tempDO1 == tempDO2))


#
#   Test only possible for BiSpinor/AdjointBiSpinor
#


def test_adjoint(Shape):
    temp_arr = build_input_array((4,), Shape, dtype=np.complex)
    BS = BiSpinor(temp_arr)
    aBS = AdjointBiSpinor(np.conjugate(temp_arr))
    AdBS = AdjointBiSpinor(temp_arr)
    aAdBS = BiSpinor(np.conjugate(temp_arr))

    assert_AlgebraObj_equal(BS.adjoint(), aBS)
    assert_AlgebraObj_equal(AdBS.adjoint(), aAdBS)


def test_vdot(S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_arrays((4,), S1, S2, dtype=np.complex)
    BS1 = BiSpinor(temp_arr1)
    BS2 = BiSpinor(temp_arr2)

    res = BS1.vdot(BS2)

    temp_arr1_conj = np.conjugate(temp_arr1)
    groundtruth = (
        temp_arr1_conj[0] * temp_arr2[0]
        + temp_arr1_conj[1] * temp_arr2[1]
        + temp_arr1_conj[2] * temp_arr2[2]
        + temp_arr1_conj[3] * temp_arr2[3]
    )

    assert_equal(res, groundtruth)
    assert_raises(TypeError, BS1.vdot, None)


#
# test matmul
#


def test_matmul_DM_DM(S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_arrays((4, 4), S1, S2)
    DM1 = DiracMatrix(temp_arr1)
    DM2 = DiracMatrix(temp_arr2)
    res = DM1 @ DM2
    res_mul = DM1 * DM2
    groundtruth = np.einsum("ij...,jk... -> ik...", temp_arr1, temp_arr2)
    assert_(isinstance(res, DiracMatrix))
    assert_(isinstance(res_mul, DiracMatrix))
    assert_almost_equal(res.data, groundtruth)
    assert_almost_equal(res_mul.data, groundtruth)


def test_matmul_DM_BS(S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_arrays((4, 4), S1, S2)
    DM1 = DiracMatrix(temp_arr1)
    BS2 = BiSpinor(temp_arr2[0, :])
    res = DM1 @ BS2
    res_mul = DM1 * BS2
    groundtruth = np.einsum("ij...,j... -> i...", temp_arr1, temp_arr2[0, :])
    assert_(isinstance(res, BiSpinor))
    assert_(isinstance(res_mul, BiSpinor))
    assert_almost_equal(res.data, groundtruth)
    assert_almost_equal(res_mul.data, groundtruth)


def test_matmul_AdBS_DM(S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_arrays((4, 4), S1, S2)
    DM1 = DiracMatrix(temp_arr1)
    AdBS2 = AdjointBiSpinor(temp_arr2[0, :])
    res = AdBS2 @ DM1
    res_mul = AdBS2 * DM1
    groundtruth = np.einsum("i...,ij... -> j...", temp_arr2[0, :], temp_arr1)
    assert_(isinstance(res, AdjointBiSpinor))
    assert_(isinstance(res_mul, AdjointBiSpinor))
    assert_almost_equal(res.data, groundtruth)
    assert_almost_equal(res_mul.data, groundtruth)


def test_matmul_AdBS_BS(S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_arrays((4,), S1, S2)
    BS1 = BiSpinor(temp_arr1)
    AdBS2 = AdjointBiSpinor(temp_arr2)
    res = AdBS2 @ BS1
    res_mul = AdBS2 * BS1
    groundtruth = np.einsum("i...,i... -> ...", temp_arr2, temp_arr1)

    # assert_(isinstance(res,np.ndarray))
    assert_almost_equal(res.data, groundtruth)
    assert_almost_equal(res_mul.data, groundtruth)


def test_matmul_BS_AdBS(S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_arrays((4,), S1, S2)
    AdBS1 = AdjointBiSpinor(temp_arr1)
    BS2 = BiSpinor(temp_arr2)
    res = BS2 @ AdBS1
    groundtruth = np.einsum("i...,j... -> ij...", temp_arr2, temp_arr1)
    assert_(isinstance(res, DiracMatrix))
    assert_almost_equal(res.data, groundtruth)


allowed_combinations = [
    [DiracMatrix, DiracMatrix],
    [DiracMatrix, BiSpinor],
    [AdjointBiSpinor, DiracMatrix],
    [AdjointBiSpinor, BiSpinor],
    [BiSpinor, AdjointBiSpinor],
]


# raises for wrong input
def test_fail_matmul(AC1, AC2):
    if not (
        np.any([np.array_equal([AC1, AC2], comb) for comb in allowed_combinations])
    ):
        temp_arr1 = np.ones(AC1.TSHAPE + tuple())
        tempDO1 = AC1(temp_arr1)
        temp_arr2 = np.ones(AC2.TSHAPE + tuple())
        tempDO2 = AC2(temp_arr2)
        assert_raises(TypeError, op.matmul, tempDO1, tempDO2)
