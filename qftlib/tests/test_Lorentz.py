"""
pytest suite for qftlib.Lorentz
"""
import numbers
import operator as op

import numpy as np
import pytest
from conftest import Shape, assert_call, build_broadcastable_arrays, build_input_array
from numpy.testing import assert_, assert_almost_equal, assert_equal, assert_raises

from qftlib.DiracAlgebra import AdjointBiSpinor, BiSpinor, DiracMatrix
from qftlib.Lorentz import LorentzVector, _LorentzVectorType


# helper function
def build_input(shape):
    return build_input_array((4,), shape)


def build_broadcastable_input(s1, s2, dtype=None):
    return build_broadcastable_arrays((4,), s1, s2, dtype)


# same as in test_DiracAlgebra
# TODO: ship to conftest.py
# class fixture
classes = [DiracMatrix, BiSpinor, AdjointBiSpinor]

classids = tuple(cls.__name__ for cls in classes)


@pytest.fixture(params=classes, ids=classids)
def AlgebraClass(request):
    return request.param


###########################
#
# Tests involving one class
#
###########################


def test_init_from_array(Shape):
    temp_arr = build_input(Shape)
    assert_call(LorentzVector, temp_arr)
    temp = LorentzVector(temp_arr)
    assert_(isinstance(temp, _LorentzVectorType))
    assert_(type(temp.__repr__()) == str)
    assert_(type(temp.__str__()) == str)

    component_names = ["x0", "x1", "x2", "x3"]
    for idx in range(4):
        assert_equal(temp_arr[idx], getattr(temp, component_names[idx]))


def test_init_from_components(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr[0], temp_arr[1], temp_arr[2], temp_arr[3])
    assert_(isinstance(temp, _LorentzVectorType))


def test_init_from_LV(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(LorentzVector(temp_arr))
    assert_(isinstance(temp, _LorentzVectorType))


def test_fail_init(Shape):
    temp_arr = build_input_array(
        (
            1,
            1,
        ),
        Shape,
    )
    assert_raises(ValueError, LorentzVector, temp_arr)
    assert_raises(ValueError, LorentzVector, None)


def test_shapes(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    assert_equal(temp.shape, Shape)


def test_shapes_NotImplemented():
    temp = LorentzVector(1, 2, 3, 4)
    assert_equal(temp.shape, NotImplemented)


def test_asarray(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    res = np.asarray(temp)
    assert_(isinstance(res, np.ndarray))
    assert_equal(res, temp_arr)


def test_reshape(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    new_shape = temp.shape + (1,)
    res = temp.reshape(*new_shape)
    assert_(isinstance(res, _LorentzVectorType))
    assert_equal(res.shape, new_shape)


def test_reshape_NotImplemented(Shape):
    temp = LorentzVector(1, 2, 3, 4)
    assert_equal(temp.reshape(tuple()), NotImplemented)


unary_methods = [
    op.neg,
    op.pos,
]
unary_method_ids = tuple(mth.__name__ for mth in unary_methods)


@pytest.fixture(params=unary_methods, ids=unary_method_ids)
def UnaryMethod(request):
    return request.param


def test_unary_methods(UnaryMethod, Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    groundtruth = UnaryMethod(temp_arr)
    res = UnaryMethod(temp)
    assert_(isinstance(res, _LorentzVectorType))
    assert_equal(np.asarray(res), groundtruth)


# test arithmetics with numbers and ndarrays
def radd(x, y):
    return op.add(y, x)


def rsub(x, y):
    return op.sub(y, x)


def rmul(x, y):
    return op.mul(y, x)


binary_array_methods = [op.mul, rmul]
binary_array_method_ids = tuple(mth.__name__ for mth in binary_array_methods)


@pytest.fixture(params=binary_array_methods, ids=binary_array_method_ids)
def BinaryArrayMethod(request):
    return request.param


def test_binary_array_methods(BinaryArrayMethod, Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    other = np.random.random(Shape)
    res = BinaryArrayMethod(temp, other)
    groundtruth = BinaryArrayMethod(np.asarray(temp), other.reshape((1,) + Shape))
    assert_(isinstance(res, _LorentzVectorType))
    assert_almost_equal(np.asarray(res), groundtruth)


def test_binary_number_methods(BinaryArrayMethod, Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    other = np.random.random()
    res = BinaryArrayMethod(temp, other)
    groundtruth = BinaryArrayMethod(np.asarray(temp), other)
    assert_(isinstance(res, _LorentzVectorType))
    assert_almost_equal(np.asarray(res), groundtruth)


binary_array_fail_methods = [op.add, op.sub, radd, rsub]
binary_array_fail_method_ids = tuple(mth.__name__ for mth in binary_array_fail_methods)


@pytest.fixture(params=binary_array_fail_methods, ids=binary_array_fail_method_ids)
def BinaryArrayFailMethod(request):
    return request.param


def test_binary_array_fail_methods(BinaryArrayFailMethod, Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    other = np.random.random(Shape)
    assert_raises(TypeError, BinaryArrayFailMethod, temp, other)


def test_binary_number_fail_methods(BinaryArrayFailMethod, Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    other = np.random.random()
    assert_raises(TypeError, BinaryArrayFailMethod, temp, other)


# test arithmetics same class
binary_same_class_methods = [op.add, op.sub]

binary_same_class_method_ids = tuple(mth.__name__ for mth in binary_same_class_methods)


@pytest.fixture(params=binary_same_class_methods, ids=binary_same_class_method_ids)
def BinarySameClassMethod(request):
    return request.param


S1 = Shape
S2 = Shape


def test_binary_same_class_methods(BinarySameClassMethod, S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_input(S1, S2)
    temp1 = LorentzVector(temp_arr1)
    temp2 = LorentzVector(temp_arr2)
    res = BinarySameClassMethod(temp1, temp2)
    groundtruth = BinarySameClassMethod(np.asarray(temp1), np.asarray(temp2))
    assert_(isinstance(res, _LorentzVectorType))
    assert_almost_equal(np.asarray(res), groundtruth)


# test arithmetics different class
binary_different_class_methods = [op.add, op.sub, op.mul, radd, rsub, rmul]

binary_different_class_method_ids = tuple(
    mth.__name__ for mth in binary_different_class_methods
)


@pytest.fixture(
    params=binary_different_class_methods, ids=binary_different_class_method_ids
)
def BinaryDifferentClassMethod(request):
    return request.param


def test_binary_different_class_methods(BinaryDifferentClassMethod):
    temp_arr = build_input(tuple())
    temp = LorentzVector(temp_arr)
    assert_raises(TypeError, BinaryDifferentClassMethod, temp, None)


# test arithmetics raise for mul(LV,LV)
def test_binary_same_class_fail_methods(S1, S2):
    temp_arr1, temp_arr2 = build_broadcastable_input(S1, S2)
    temp1 = LorentzVector(temp_arr1)
    temp2 = LorentzVector(temp_arr2)
    assert_raises(TypeError, op.mul, temp1, temp2)


# test equality


def test_equal_different_class():
    temp_arr = np.ones((4,))
    temp = LorentzVector(temp_arr)
    assert_(not (temp == 5))
    assert_(temp != 5)


def test_equal_same_class(S1, S2):
    temp_arr1 = np.ones((4,) + S1)
    temp1 = LorentzVector(temp_arr1)
    temp_arr2 = np.ones((4,) + S2)
    temp2 = LorentzVector(temp_arr2)
    res_eq = temp1 == temp2
    res_neq = temp1 != temp2
    print(res_neq)
    if np.array_equal(S1, S2):
        assert_(res_eq)
        assert_(not (res_neq))
    else:
        assert_(res_neq)
        assert_(not (res_eq))


# test matmul
def test_matmul(S1, S2):
    METRIC = np.diag([1, -1, -1, -1])
    temp_arr1, temp_arr2 = build_broadcastable_input(S1, S2)
    LV1 = LorentzVector(temp_arr1)
    LV2 = LorentzVector(temp_arr2)
    res = LV1 @ LV2
    groundtruth = np.einsum("ij,i...,j... -> ...", METRIC, temp_arr1, temp_arr2)
    assert_(isinstance(res, np.ndarray) or isinstance(res, numbers.Number))
    assert_almost_equal(np.asarray(res), groundtruth)


def test_fail_matmul():
    temp_arr = np.ones(4)
    temp = LorentzVector(temp_arr)
    assert_raises(TypeError, op.matmul, temp, None)


# test interface with dirac algebra
def test_init_with_DiracAlgebra(AlgebraClass, Shape):
    temp_arr = build_input_array(AlgebraClass.TSHAPE, Shape)
    LV_input = [AlgebraClass(temp_arr) for _ in range(4)]
    LV = LorentzVector(*LV_input)

    # type check
    assert_(isinstance(LV.x0, AlgebraClass))
    assert_(isinstance(LV.x1, AlgebraClass))
    assert_(isinstance(LV.x2, AlgebraClass))
    assert_(isinstance(LV.x3, AlgebraClass))

    # value check
    assert_equal(np.array(LV.x0), temp_arr)
    assert_equal(np.array(LV.x1), temp_arr)
    assert_equal(np.array(LV.x2), temp_arr)
    assert_equal(np.array(LV.x3), temp_arr)


allowed_combinations = [
    [DiracMatrix, DiracMatrix],
    [DiracMatrix, BiSpinor],
    [AdjointBiSpinor, DiracMatrix],
    [AdjointBiSpinor, BiSpinor],
    [BiSpinor, AdjointBiSpinor],
]

A1 = AlgebraClass
A2 = AlgebraClass


def test_mul_with_DiracAlgebra(A1, A2, Shape):
    if np.all([np.array_equal([A1, A2], comb) for comb in allowed_combinations]):
        temp_arr = build_input_array(A1.TSHAPE, Shape)
        LV_input = [A1(temp_arr) for _ in range(4)]
        fac = A2(temp_arr)
        LV = LorentzVector(*LV_input)
        resMul = LV * fac
        resRMul = fac * LV
        groundtruth = LorentzVector(*[el * fac for el in LV_input])
        assert_(isinstance(resMul, _LorentzVectorType))
        assert_(resMul == groundtruth)
        assert_(resRMul is NotImplemented)
