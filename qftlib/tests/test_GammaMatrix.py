"""
Test suites for Gamma Matrices.
"""
import numpy as np
import pytest
from conftest import build_input_array
from numpy.testing import assert_, assert_almost_equal, assert_equal

from qftlib import (
    G0,
    G1,
    G2,
    G3,
    G5,
    AdjointBiSpinor,
    BiSpinor,
    DiracMatrix,
    Gmu,
    LorentzVector,
    feynman_slash,
)

cases = [G0, G1, G2, G3]

caseids = tuple(i for i, _ in enumerate(cases))


@pytest.fixture(params=cases, ids=caseids)
def Gammas(request):
    return request.param


# helper function
def build_input(shape):
    return build_input_array((4,), shape)


# additional asserts
# TODO: ship this to conftest.py
def assert_AlgebraObj_equal(DO1, DO2, msg=""):
    try:
        assert_(
            isinstance(DO1, DiracMatrix)
            or isinstance(DO1, BiSpinor)
            or isinstance(DO1, AdjointBiSpinor)
        )
        assert_(
            isinstance(DO2, DiracMatrix)
            or isinstance(DO2, BiSpinor)
            or isinstance(DO2, AdjointBiSpinor)
        )
        assert_(DO1.__class__ == DO2.__class__)
        assert_equal(DO1.data, DO2.data)
    except AssertionError:
        msg = f"Result {DO1}\nTarget {DO2}"
        raise AssertionError(msg)


def assert_AlgebraObj_almost_equal(DO1, DO2, msg=""):
    try:
        assert_(
            isinstance(DO1, DiracMatrix)
            or isinstance(DO1, BiSpinor)
            or isinstance(DO1, AdjointBiSpinor)
        )
        assert_(
            isinstance(DO2, DiracMatrix)
            or isinstance(DO2, BiSpinor)
            or isinstance(DO2, AdjointBiSpinor)
        )
        assert_(DO1.__class__ == DO2.__class__)
        assert_almost_equal(DO1.data, DO2.data)
    except AssertionError:
        msg = f"Result {DO1}\nTarget {DO2}"
        raise AssertionError(msg)


# single gamma tests
def test_gamma_vector():
    assert_(isinstance(G0, DiracMatrix))
    assert_AlgebraObj_equal(Gmu.x0, G0)
    assert_(isinstance(G1, DiracMatrix))
    assert_AlgebraObj_equal(Gmu.x1, G1)
    assert_(isinstance(G2, DiracMatrix))
    assert_AlgebraObj_equal(Gmu.x2, G2)
    assert_(isinstance(G3, DiracMatrix))
    assert_AlgebraObj_equal(Gmu.x3, G3)


def test_hermite():
    DA_eye = DiracMatrix(np.eye(4))
    assert_AlgebraObj_almost_equal(G0 * G0, DA_eye)
    assert_AlgebraObj_almost_equal(G1 * G1, -DA_eye)
    assert_AlgebraObj_almost_equal(G2 * G2, -DA_eye)
    assert_AlgebraObj_almost_equal(G3 * G3, -DA_eye)


# tests with two gamma matrices


# tests with all gamma matrices
def test_gamma5():
    res = 1j * G0 * G1 * G2 * G3
    assert_AlgebraObj_almost_equal(res, G5)


gamma1 = Gammas
gamma2 = Gammas
METRIC = np.diag([1, -1, -1, -1])
EYE = np.eye(4)

gamma_anti_commutator = 2 * METRIC.reshape(4, 4, 1, 1) * EYE.reshape(1, 1, 4, 4)


# ship this to utils?
def anti_commutator(M1, M2):
    return M1 @ M2 + M2 @ M1


def test_clifford_algebra(gamma1, gamma2):
    res = anti_commutator(gamma1, gamma2)
    if gamma1 != gamma2:
        groundtruth = DiracMatrix(np.zeros((4, 4)))
    elif gamma1 == G0:
        groundtruth = DiracMatrix(2 * np.eye(4))
    else:
        groundtruth = DiracMatrix(-2 * np.eye(4))
    assert_AlgebraObj_almost_equal(res, groundtruth)


def test_feyn_slash_type(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    temp_sl = feynman_slash(temp)
    assert_(isinstance(temp_sl, DiracMatrix))


def test_feyn_slash_square(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    temp_sl = feynman_slash(temp)
    res = temp_sl @ temp_sl
    groundtruth = DiracMatrix(
        (temp @ temp).reshape((1, 1) + Shape)
        * np.eye(4).reshape((4, 4) + (1,) * len(Shape))
    )
    assert_AlgebraObj_almost_equal(res, groundtruth)


def test_feyn_slash_gamma_sandwitch(Shape):
    temp_arr = build_input(Shape)
    temp = LorentzVector(temp_arr)
    temp_sl = feynman_slash(temp)
    res = Gmu @ (temp_sl * Gmu)
    groundtruth = -2 * temp_sl
    assert_AlgebraObj_almost_equal(res, groundtruth)
