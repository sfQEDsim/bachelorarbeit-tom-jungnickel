"""
configuration and fixtures for pytest
"""
import numpy as np
import pytest

SEED = 12345678

# shape fixture -> maybe ship to a general test module!

N = 2
shapes = [tuple(), (N,), (N, N), (N, N, N)]


@pytest.fixture(params=shapes)
def Shape(request):
    return request.param


# asserts that a call runs as expected
def assert_call(fct, *args, **kwargs):
    try:
        fct(*args, **kwargs)
    except Exception as e:
        print(type(e))
        raise AssertionError(
            f"The function was not called properly. It raised the exception:\n\n {e.__class__.__name__}: {e}"
        )


def build_input_array(tshape, shape, dtype=None):
    if dtype in (None, np.float):
        return np.random.random(tshape + shape)
    elif dtype is np.complex:
        return build_input_array(tshape, shape) + 1j * build_input_array(tshape, shape)
    else:
        raise ValueError(
            "The dtype {dtype} is not supported. (Use dtype = np.float or dtype = np.complex instead)"
        )


def build_broadcastable_arrays(tshape, shape1, shape2, dtype=None):
    """
    Builds two broadcastable arrays by adding ones to the smaller shape.

    !!!All axes need to have the same length!!!
    """
    if len(shape1) == len(shape2):
        return build_input_array(tshape, shape1, dtype=dtype), build_input_array(
            tshape, shape2, dtype=dtype
        )
    elif len(shape1) > len(shape2):
        s1, s2 = shape1, shape2
    else:
        s2, s1 = shape1, shape2

    s2_broadcast = (1,) * (len(s1) - len(s2)) + s2
    return build_input_array(tshape, s1, dtype=dtype), build_input_array(
        tshape, s2_broadcast, dtype=dtype
    )


def build_rnd_momentum_components(shape=None, seed=None):
    """Builds random onshell components for a FourMomentum.

    Here we do not build the FourMomentum itself, since in conftest.py, there shall be no code to  be tested.

    """
    if seed is None:
        seed = SEED
    if shape is None:
        shape = tuple()
    np.random.seed(seed)

    m = np.random.random()
    px = np.random.random(shape)
    pz = np.random.random(shape)
    py = np.random.random(shape)
    E = np.sqrt(px ** 2 + py ** 2 + pz ** 2 + m ** 2)

    return np.array([E, px, py, pz]), m
