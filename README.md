# BachelorarbeitTomJungnickel

Collection of the code to produce the bachelor thesis of Tom Jungnickel.

**Required packages**
- qftlib

Installation: Go to the `qftlib` directory and execute
`pip install -e .[all,dev,docs]`

- simplemc

Installation: Go to the `simplemc` directory and execute
`pip install -e .`

Further instructions can be found [here](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/tree/main/simplemc).


**Authorship**

Uwe Hernandez Acosta: [qftlib](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/tree/main/qftlib), [simplemc](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/tree/main/simplemc), [event_generator](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/blob/main/04-event_generator_test.ipynb)

Tom Jungnickel: [field_reduction](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/blob/main/field_reduction.py), [event_generator](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/blob/main/04-event_generator_test.ipynb); modifications in [event_generation](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/blob/main/simplemc/src/simplemc/event_generation.py), [event_records](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/blob/main/simplemc/src/simplemc/event_records.py), [field_records](https://gitlab.hzdr.de/sfQEDsim/bachelorarbeit-tom-jungnickel/-/blob/main/simplemc/src/simplemc/field_records.py)
