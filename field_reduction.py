import numpy as np
import qftlib as qft
import simplemc
import attr
import matplotlib.pylab as plt
from scipy.integrate import quad


# Some functions
def base_function(x, A, mu, sigma):
    """The starting |F(l)|^2 with constant factor A (does not include zeta in case of full scaling)
    """
    return abs(A * np.exp(-0.5 * ((x-mu) / sigma)**2))**2

def delta(x, l):
    """Simple delta distribution (0 width)
    Because integrating this function will pass a float as x but plotting will pass an array, 
    the x==l will fail in the second case and iterating over x will fail in the first case.
    (maybe not the best) solution: check the type of x
    """
    # Float case
    if isinstance(x, np.float):
        if x == l:
            return 1000
        else:
            return 0
    # Array case
    elif isinstance(x, np.ndarray):
        temp = np.zeros_like(x)
        for n, xi in enumerate(x):
            if xi == l:
                temp[n] = 1000
        return temp 
    else:
        raise AttributeError('Unexpected type of x')

def dirac_series(x, epsilon, l):
    """Normal distribution * l to be substracted from spectrum
    """
    return l * 1/(2 * np.pi * epsilon)**0.5 * np.exp(-(x-l)**2 / (2*epsilon))


# Field updating methods
def delta_hole(field, x):
    """Method to reduce the field spectrum by substracting delta functions for each collected l
    """
    temp = base_function(x, field.A, field.mu, field.sigma)
    for i in field.l_array:
        for j in i:
            temp = temp - delta(x, j)
    return temp

def dirac_hole(field, x):
    """Method to reduce the field by substracting multiple dirac series (centered around the respective l)
    todo:
    - scale the curves according to l
    """
    temp = base_function(x, field.A, field.mu, field.sigma)
    for i in field.l_array:
        for j in i:
            temp = temp - dirac_series(x, field.epsilon, j)
    return temp

def full_scaling(field, x):
    """Method to reduce the field by reducing its amplitude
    """
    return field.zeta_tot * base_function(x, field.A, field.mu, field.sigma)


@attr.s
class spectrum:
    """Generic spectrum |F(l)|^2 and its reduction methods
    """
    A = attr.ib()        # constant in front of the gauss in F(l)
    mu = attr.ib()       # = 1, center of the gauss
    sigma = attr.ib()    # = sigma_l
    method = attr.ib()   # full scaling or 2 types of hole burning
    omega = attr.ib()    # = k_z of the field
    epsilon = attr.ib(default=0.01)   # width parameter of the dirac series
    l_array = attr.ib(default=[])     # stores all the non 0 energy fractions (array of arrays of floats)
    zeta_tot = attr.ib(default=1.0)   # total reduction factor in full_scaling

    def __call__(self, x):
        if self.method == 2:
            return delta_hole(self,x)
        elif self.method == 1:
            return dirac_hole(self,x)
        elif self.method == 0:
            return full_scaling(self,x)
        else:
            raise AttributeError('Invalid field reduction method specified')

    def update(self, event):
        """Remove energy from the field if a new particle was generated and then check if field < 0
        """
        # Updating part
        l_new = event.en_fracs[event.en_fracs != 0]

        # < 0 checking part
        if not l_new:
            # Skip generations with no new particles
            return
        else:
            self.l_array.append(l_new)

            if self.method == 2:
                """Hole burning (delta distribution)
                """
                E_out = self.calculate_energy(event.in_field)
                if E_out < 0:
                    print('Stopping because E < 0')
                    event.stop_flag = True

            elif self.method == 1:
                """Hole burning (dirac series)
                Reduced field is calculated from all the collected l and searched for roots (inside the bounds)
                step size should be smaller than epsilon so no roots get skipped
                """
                step_size = self.epsilon / 10
                x_min = event.in_field.spectrum_bounds[0]
                x_max = event.in_field.spectrum_bounds[1]
                xi = np.arange(x_min, x_max, step_size)
                x_root = []
                # Primitive root finding which works works for now, unlike scipy.optimize.fsolve
                for i in range(len(xi)-1):
                    if (self(xi[i])*self(xi[i+1])) < 0:
                        x_root.append( (xi[i] + xi[i+1]) / 2)
                if x_root != []:
                    print(f'Found roots: {x_root}, stopping generator')
                    event.stop_flag = True

            elif self.method == 0:
                """Full scaling
                Since only the constant factor gets reduced here, no self__call__ is needed to check if < 0
                Multiple zeta in 1 generation could just be multiplied by np.prod(),
                but we want to check for a depleted field by checking if zeta becomes < 0.
                If we do that check after multiplying 2 negative zeta (could happen with trident), it will fail.
                """
                zeta = 1 - (1/event.in_field.total_energy) * l_new
                for i in zeta:
                    self.zeta_tot = self.zeta_tot * i
                    if i < 0:
                        print('Stopping because E < 0')
                        event.stop_flag = True
        
            else:
                raise AttributeError('Invalid field reduction method specified')
    
    def calculate_energy(self, field_rec):
        """Calculate the integrated spectrum U (to be replaced by energy later)
        todo:
        - pass just the bounds instead of field_rec
        """
        res,err = quad(self, *field_rec.spectrum_bounds)
        E = res
        if self.method == 2:
            # Manually subtract the delta contributions since quad ignores them
            for i in self.l_array:
                for j in i:
                    E = E - j
            return E
        else:
            return E
